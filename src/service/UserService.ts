import { ref } from 'vue'
import type { Ref } from 'vue'
import type IUser from '@/interface/IUser'

const url = import.meta.env.VITE_API_URL || '172.16.107.120:3000'

export default class AuthService {
  private users: Ref<IUser[]>
  private user: Ref<IUser>

  constructor() {
    this.users = ref([])
    this.user = ref({}) as Ref<IUser>
  }

  getUsers(): Ref<IUser[]> {
    return this.users
  }

  getUser(): Ref<IUser> {
    return this.user
  }

  async fetchAll(): Promise<void> {
    try {
      const response = await fetch(url + '/users')
      const data = await response.json()
      this.users.value = await data
    } catch (error) {
      console.log(error)
    }
  }

  async fetchPost(id: string): Promise<void> {
    try {
      const response = await fetch(url + '/user/' + id)
      const data = await response.json()
      this.user.value = await data
    } catch (error) {
      console.log(error)
    }
  }

  async createUser(email: string, name: string, password: string) {
    const response = await fetch(
      url + '/register?' + 'email=' + email + '&name=' + name + '&password=' + password,
      {
        method: 'POST'
      }
    )
    const result = await response.json()
    console.log(result)
  }
}
